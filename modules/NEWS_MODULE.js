function createBasicNews(userId, senderId, senderSocialInfo)
{
    var news = {}
    news["UserId"] = userId;
    news["SenderId"] = senderId;
    news["SenderSocialInfo"] = senderSocialInfo;
    news["Date"] = Date.now();
    
    return news;
}

function createAttackNews(userId, senderId, senderSocialInfo, building, defense)
{
    var news = createBasicNews(userId, senderId, senderSocialInfo)
    news["type"] = "AttackNews";
    news["BuildingId"] = building;
    news["Defense"] = defense;
    
    var collection = Spark.runtimeCollection("newsfeed");
    collection.insert(news);
}

function createStealNews(userId, senderId, senderSocialInfo, cash)
{
    var news = createBasicNews(userId, senderId, senderSocialInfo)
    news["type"] = "StealNews";
    news["Cash"] = cash;
    
    var collection = Spark.runtimeCollection("newsfeed");
    collection.insert(news);
}