function UpdateRevenge(id, socialInfo, exp, hasBuildings, target, key, value)
{
    var json = {};
    json["Revenges.".concat(id).concat(".Id")] = id;
    json["Revenges.".concat(id).concat(".SocialInfo")] = (socialInfo)? socialInfo : {};
    json["Revenges.".concat(id).concat(".Exp")] = exp;
    json["Revenges.".concat(id).concat(".HasBuildings")] = hasBuildings;
    json["Revenges.".concat(id).concat(".MayAttack")] = true;
    json["Revenges.".concat(id).concat(".LastInteraction")] = Date.now();
    
    var collection = Spark.runtimeCollection("users");
    
    var query =
    {
        "_id": { $eq: {"$oid": target}},
    };
    
    var jsonKey = "Revenges.".concat(id).concat(".").concat(key);
    
    var inc = {};
    inc[jsonKey] = value;
    
    var update ={"$set":json, "$inc":inc};
    
    var result = collection.update
    (
        query,
        update
    )
    
    return result;
}

function DisableRevenge(id, target)
{
   var collection = Spark.runtimeCollection("users");
    
    var query = {};
    query["_id"] = { $eq: {"$oid": id}},
    query["Revenges.".concat(target)] = { "$exists" : true };
     
     var update = {};
     update["$set"] = {};
     update.$set["Revenges.".concat(target).concat(".MayAttack")] = false ;
     
     collection.update(query, update);
}

function UpdateExistentRevenge(target, exp, hasBuildings, myId)
{
    var json = {};
    json["Revenges.".concat(id).concat(".Id")] = target;
    json["Revenges.".concat(id).concat(".Exp")] = exp;
    json["Revenges.".concat(id).concat(".HasBuildings")] = hasBuildings;
    
    var collection = Spark.runtimeCollection("users");
    
    var query = {};
    query["_id"] = { $eq: {"$oid": id}};
    query["Revenges.".concat(target)] = {"$exits" : true};
    
    var update ={"$set":json};
}