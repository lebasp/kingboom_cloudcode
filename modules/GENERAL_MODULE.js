function GetCashKing(id, min, max)
    {
     
    var metaJson = Spark.metaCollection("db").findOne();
    var totalIsland = metaJson.TotalIslands;
     
    var defaultCash = Math.floor(Math.random() * max) + min;
    
    var collection = Spark.runtimeCollection("users");
    
    var user = collection.findOne({"_id": { $eq: {"$oid": id} }}, {LastLogin: 1, CashKing:1});
    
    var resetCashKing = false;
    
    var ckJson = {};
    ckJson.Id = "dummy";
    ckJson.Cash = defaultCash * 0.85;
    ckJson.Island = 
            {
                "Id": Math.floor(Math.random() * totalIsland) + 0,
                "Buildings": 
                    [
                        Math.floor(Math.random() * 5) + (0),
                        Math.floor(Math.random() * 5) + (0),
                        Math.floor(Math.random() * 5) + (0),
                        Math.floor(Math.random() * 5) + (0),
                        Math.floor(Math.random() * 5) + (0)
                    ],
                "Damaged":
                    [
                        Math.random() < 0.1 ? true : false,
                        Math.random() < 0.1 ? true : false,
                        Math.random() < 0.1 ? true : false,
                        Math.random() < 0.1 ? true : false,
                        Math.random() < 0.1 ? true : false
                    ]
            };
    ckJson.SocialInfo = {};
    ckJson.SocialInfo.FacebookId = "";
    ckJson.SocialInfo.DisplayName = "dummy";
    
    if(user && "CashKing" in user)
    {
        var previousDate = new Date(0);
        previousDate.setMilliseconds(user.LastLogin);
        
        var dateNow = new Date(0);
        dateNow.setMilliseconds(Date.now());
        
        ckJson.Id = user.CashKing.Id;
        ckJson.Cash = user.CashKing.Cash;
        ckJson.Island = user.CashKing.Island;
            
        if(dateNow.getDay() !== previousDate.getDay())
            resetCashKing = true; 
    }
    else
        resetCashKing = true;
        
    resetCashKing = true;
        
    if(resetCashKing)
    {
       var result = collection.findOne(
        {
                "_id" : { $ne: {"$oid": id} },
                Random : { $near : [Math.random(), Math.random()] },
                "UserResources.Cash" : {$gt : min, $lt : max},
        },
        {UserResources:1, ActiveIsland:1, SocialInfo:1});
        
        if(result !== null)
        {
            ckJson.Id = result._id.$oid;
            ckJson.Cash = result.UserResources.Cash * 0.85;
            ckJson.Island = result.ActiveIsland;
            ckJson.SocialInfo = result.SocialInfo;
        }
    }
    
    return ckJson;
    
}