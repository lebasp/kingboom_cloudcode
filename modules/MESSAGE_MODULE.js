function sendMessage(target,json)
{
//    var playerIds = [target];
    
    Spark.message("test");
    
    var msg = Spark.message(null);
    msg.setPlayerIds([ Spark.loadPlayer(target).getPlayerId() ]);
    msg.setMessageData(json);
    msg.setSupressPushOnSocketSend(false);
    msg.send();
    
   // Spark.message("test").setPlayerIds(playerIds).setMessageData(json).setSendAsPush(true).send();
}

var Scheduler = 
{
    schedule: function(eventShortCode, taskId, delayInSeconds, parameters)
    {
        var taskData =
        {
            "id":taskId,
            "target": parameters.target,
            "json" : parameters.json
        };
        
        var collection = Spark.runtimeCollection("scheduledTask");
        var result = collection.findOne({"id":taskId});
        
        if (result === null)
        {
            collection.insert(taskData);
        }
        else
        {
            Spark.getScheduler().cancel(taskId);
        }

        Spark.getScheduler().inSeconds(eventShortCode, delayInSeconds, taskData, taskId);
    },
    
    finish: function(taskId)
    {
        var taskData = Spark.runtimeCollection("scheduledTask").findOne({"id":taskId});
        
        if (taskData !== null)
        {
            Spark.getScheduler().cancel(taskId);
            Spark.runtimeCollection("scheduledTask").remove({"id":taskId});
        }
    }
}