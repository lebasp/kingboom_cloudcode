
var id = Spark.getData().id;

var collection = Spark.runtimeCollection("users");

var query = { "_id": { $eq: {"$oid": id} }};

var result = collection.findOne(query, { "Revenges": 1 });

var revenges = result["Revenges"];
for(var key in revenges)
{
    if(!revenges[key].MayAttack)
    {
        delete revenges[key];
    }
}


//foreach on dictionary to return the may attack true only


/*list documents
var result = collection.aggregate(
    { $match: { "_id": { $eq: {"$oid": id} }  } },
     {
      $project:
      {
         "Revenges":
         {
            $filter: 
            {
               input: "$Revenges",
               as: "item",
               cond: { "$eq": [ "$$item.MayAttack", true ] }
            }
         }
      }
   }
    );

var obj = result;
*/

Spark.setScriptData("result", result)