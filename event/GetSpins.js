var id = Spark.getData().id;

var collection = Spark.runtimeCollection("users");

var json = {}
json["NextSpinEarned"] = 1;
json ["UserResources.Spins"] = 1;
json ["GameStatus"] = 1;

var query =  {"_id": { $eq: {"$oid": id} }};

var result = collection.findOne(query, json);

var spins = 0;
var nextEarned = Date.now();
var gameStatus = 0;

if(result)
{
    if(result.UserResources && result.UserResources.Spins)
        spins = result.UserResources.Spins;
        
    if(result.NextSpinEarned !== null)
        nextEarned = result.NextSpinEarned;
        
    gameStatus = result.GameStatus;
}

Spark.setScriptData("Spins", spins );
Spark.setScriptData("NextSpinEarned", nextEarned );
Spark.setScriptData("CurrentTime", Date.now() );
Spark.setScriptData("GameStatus", gameStatus);