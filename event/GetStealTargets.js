var id = Spark.getData().id;

var collection = Spark.runtimeCollection("users");

var query =  
{
        "_id": { $eq: {"$oid": id} }
};

var result = collection.findAndModify(query, {CashKing:1}, {}, false, {"$set":{"GameStatus": 220}}, true, false);

if(result.CashKing)
{
    var ckKey = result.CashKing.Id;
    var ckCash = result.CashKing.Cash;
    var ckSocial = result.CashKing.SocialInfo;

    result.CashKing.Cash = result.CashKing.Cash;
    Spark.setScriptData("CashKing", result.CashKing);;
    
    /*
    //ck island
    if(ckKey !== "dummy")
    {
        var ckIslandResult = collection.findOne({ "_id": { $eq: {"$oid": ckKey} }}, {ActiveIsland:1, SocialInfo:1});
        
        ckIsland = ckIslandResult.Island;
    }
    else
        ckIsland = result.CashKing.Island;
    */
    Spark.setScriptData("CashKingIsland", result.CashKing.Island);

    var query2 =  {}
    if(id !== "dummy")
        query2["_id"] = { $ne: {"$oid": id} };
    
    var gt = Math.max(100000, ckCash * 0.4);
        var lt = ckCash * 0.6;
    
    query2["Random"] = { $near : [Math.random(), Math.random()] };
    query2["UserResources.Cash"] = {$exists:true, $gt : gt, $lt : lt};
    query2["ActiveIsland"] = {$exists : true };
    
    
    var others = collection.find(query2, {UserResources:1, ActiveIsland:1, SocialInfo: 1}).limit(2);
    
    var result = [];
    
     while(others.hasNext())
     {
         var other =  others.next();
        other.UserResources.Cash = other.UserResources.Cash * 0.85;
        
        result.push(other);
     }
    
    Spark.setScriptData("Others", result);
}



