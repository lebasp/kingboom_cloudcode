var id = Spark.getData().id;

var collection = Spark.runtimeCollection("users");

var query =  {"_id": { $eq: {"$oid": id} }};

var result = collection.findOne(query, {"UserResources.Cash" : 1, "Treasures" : 1});

Spark.setScriptData("result", result );