var metaDb = Spark.metaCollection("promotions");

var today = Date.now();

var query = {
             "StartDate" : { "$lt" :today },
             "EndDate" : { "$gt" : today },
            };

var result = metaDb.find(query);


Spark.setScriptData("promotions", result);