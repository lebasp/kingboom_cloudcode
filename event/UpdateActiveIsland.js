var id = Spark.getData().id;
var json = Spark.getData().json;

var island = json.ActiveIsland;
var cash = json.Cash;
var exp = json.Exp;

var collection = Spark.runtimeCollection("users");

var query = {"_id": { $eq: {"$oid": id} }};
var update = {};
update["$set"] = {};
update.$set["ActiveIsland"] = island;

update["$inc"] = {};
update.$inc["UserResources.Exp"] = exp;
update.$inc["UserResources.Cash"] = cash;
update.$inc["Treasures.MoneySpent"] = Math.abs(cash);

var result = collection.findAndModify(query, {"UserResources" : 1, "Treasures" : 1}, {}, false, update, true, true);

Spark.setScriptData("UserResources", result.UserResources);
Spark.setScriptData("Treasures", result.Treasures);
