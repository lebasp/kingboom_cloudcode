require("GENERAL_MODULE");


var id = Spark.getData().id;
var json = Spark.getData().json;

var socialInfo = json.SocialInfo;

var collection = Spark.runtimeCollection("users");

var updateData = {};
updateData["$set"] = {};
updateData.$set["LastLogin"] = Date.now();

//update random
var result = collection.findOne({"_id": { $eq: {"$oid": id}}});

if((result === null) || !("Random" in result))
    updateData.$set["Random"] =[Math.random(),Math.random()];

if((result === null) || !("SocialInfo" in result) || socialInfo.FacebookId !== "")
    updateData.$set["SocialInfo"] = socialInfo;

if((result === null) || !("WorkersBank" in result))
    updateData.$set["WorkersBank"] = {};

if((result === null) || !("UserResources" in result))
{
    var metaJson = Spark.metaCollection("db").findOne();
    
    
    updateData.$set["UserResources.Spins"] = metaJson.NewUserValues.spins;
    updateData.$set["UserResources.Cash"] = metaJson.NewUserValues.coins;
    updateData.$set["Treasures.Keys"] = metaJson.NewUserValues.keys;
    updateData.$set["Treasures.MoneySpent"] = 0;
}

if((result === null) || !("GameStatus" in result))
    updateData.$set["GameStatus"] = 0;

    

GetCashKing(id, 100000, 300000);

var fields = {
                "Revenges" : 1,
                "UserResources" : 1,
                "ActiveIsland" : 1,
                "Treasures" : 1,
                "WorkersBank" : 1,
                "CompletedIsland" : 1,
                "CashKing" : 1,
                "GameStatus" : 1
             };


var result = collection.findAndModify({"_id": { $eq: {"$oid": id} }}, fields, {}, false, updateData, true, true);

if(result !== null && "Revenges" in result)
{
    var revenges = result.Revenges;
    
    var updated = false;
    
    for(var key in revenges)
    {
        var previousDate = new Date(0);
        previousDate.setMilliseconds(revenges[key].LastInteraction);
    
        var dateNow = new Date(0);
        dateNow.setMilliseconds(Date.now());
        
        if((previousDate.getTime() + 604800000) < dateNow.getTime())
        {
            updated = true;
            delete revenges[key];
        }
    }
    
    if(updated)
        collection.update({"_id": { $eq: {"$oid": id} }}, {$set : { "Revenges" : revenges }});
}

result.Treasures["CurDate"] = Date.now();
result.WorkersBank["CurrentDate"] = Date.now();

Spark.setScriptData("UserResources", result.UserResources);
Spark.setScriptData("ActiveIsland", result.ActiveIsland);
Spark.setScriptData("Treasures", result.Treasures);
Spark.setScriptData("WorkersBank", result.WorkersBank);
Spark.setScriptData("CompletedIsland", result.CompletedIsland);
Spark.setScriptData("CashKing", result.CashKing);
Spark.setScriptData("GameStatus", result.GameStatus);

//remove user old news
var newsfeed = Spark.runtimeCollection("newsfeed");
newsfeed.remove({"UserId" : id, "Date" : {"$lt" : (Date.now() - 86400000)}});