var id = Spark.getData().id;
var key = Spark.getData().key;

var collection = Spark.runtimeCollection("users");

var json = {}
json[key] = 1;
json["GameStatus"] = 1;

var query =  {"_id": { $eq: {"$oid": id} }};

var result = collection.findOne(query, json);

Spark.setScriptData(key, result[key] );
Spark.setScriptData("CurDate", Date.now() );
Spark.setScriptData("GameStatus", result.GameStatus );