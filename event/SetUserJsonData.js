var id = Spark.getData().id;
var key = Spark.getData().key;
var value = Spark.getData().value;
var json = Spark.getData().json;

if(key !== "null")
{
    var json = {};
    json[key] = value;
}

var collection = Spark.runtimeCollection("users");
var query =  {"_id": { $eq: {"$oid": id} }};

var update = {"$set":json};


collection.update(query, update, true, false);