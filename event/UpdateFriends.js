var id = Spark.getData().id;
var fbids = Spark.getData().json.fbids;

var collection = Spark.runtimeCollection("users");

var resultMe = collection.findOne({"_id": { $eq: {"$oid": id} }}, {"Friends" : 1, "SocialInfo" : 1});

var resultUsers = collection.find( { "SocialInfo.FacebookId": { $in: fbids } }, {"SocialInfo" : 1, "Friends" : 1});

var friends = {};
if("Friends" in resultMe)
    friends = resultMe.Friends;



while(resultUsers.hasNext())
{
    var doc = resultUsers.next();

    if("SocialInfo" in doc)
    {
        var docId = doc._id.$oid;
            
        if(!(docId in friends))
        {
            friends[docId] = {};
            friends[docId] = {
                                "Id" : docId,
                                "LastTimeSent" : new Date(0),
                                "HasEnergy" : false,
                                "SocialInfo" : doc.SocialInfo
                             };
        }
        else
        {
            friends[docId].SocialInfo = doc.SocialInfo;
        }
    }
    
    if(!("Friends" in doc) || !(id in doc.Friends))
    {
        var updateAddFriend = {};
        updateAddFriend["$set"] = {};
        updateAddFriend.$set["Friends.".concat(id)] =  {
                                        "Id" : id,
                                        "LastTimeSent" : new Date(0),
                                        "HasEnergy" : false,
                                        "SocialInfo" : resultMe.SocialInfo
                                        };
                                        
        collection.update({"_id" : {"$eq": {"$oid": docId}} }, updateAddFriend);
    }
}
        

var result = collection.findAndModify({"_id": { $eq: {"$oid": id} }}, {"Friends" : 1}, {}, false, {"$set":{"Friends" : friends}}, true, true);

Spark.setScriptData("Friends", friends);