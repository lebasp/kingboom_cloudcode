require("GENERAL_MODULE");

var id = Spark.getData().id;
var min = Spark.getData().intValue1;
var max = Spark.getData().intValue2;

var ckJson = GetCashKing(id, min, max);

var collection = Spark.runtimeCollection("users");

collection.update({"_id": { $eq: {"$oid": id} } }, {$set: {"CashKing" : ckJson}}, true, false);  

Spark.setScriptData("CashKing", ckJson);