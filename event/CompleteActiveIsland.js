var id = Spark.getData().id;
var islandNumber = Spark.getData().intValue;

var collection = Spark.runtimeCollection("users");

var query =  {"_id": { $eq: {"$oid": id} }};

var completedIslandId = "CompletedIsland.".concat(islandNumber);

var activeIsland = {};
activeIsland["Id"] = (islandNumber+1);
activeIsland["Buildings"] = [-1, -1, -1, -1, -1 ];
activeIsland["Damaged"] = [false, false, false, false, false];

var completedIsland = {};
completedIsland["Id"] = islandNumber;
completedIsland["Workers"] = 0;
completedIsland["FriendsWorkers"] = [];

var secret = {};
secret["Id"] = 0;
secret["TriedBuildings"] = [false, false, false, false, false];
secret["TriesLeft"] = 3;

var update = {};
update["$set"] = {};

update.$set["ActiveIsland"] = activeIsland;
update.$set["CompletedIsland.".concat(islandNumber)] = completedIsland;
update.$set["Secret"] = secret;

var obj = update;

collection.update(query, update);