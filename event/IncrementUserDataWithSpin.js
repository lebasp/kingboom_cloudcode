var id = Spark.getData().id;
var key = Spark.getData().key;
var value = Spark.getData().value;

var collection = Spark.runtimeCollection("users");
var query =  {
                "_id": { $eq: {"$oid": id} },
                "UserResources.Spins" : {$gt : 0},
             };

var json = {};
if(key === "UserResources.Spins")
{
    json["UserResources.Spins"] = (value-1);
    json["Stats.TotalSpins"] = value;
}
else
{
    json[key] = value;
    json["UserResources.Spins"] = -1;
}

var update = {"$inc":json};

var result = collection.findAndModify(query, {"UserResources" : 1}, {}, false, update, true, false);

if(result === null)
    throw "NOSPINS"
else
    Spark.setScriptData("result", result.UserResources);