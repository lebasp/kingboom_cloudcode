require("REVENGE_MODULE"); 

var id = Spark.getData().id;
var target = Spark.getData().key

var query = {};

var collection = Spark.runtimeCollection("users");

if(!target)
{
    query = {
                "_id": { $ne: {"$oid": id} },
                Random : { $near : [Math.random(), Math.random()] },
                "ActiveIsland" : {$exists:true},
                "ActiveIsland.Buildings" : {$elemMatch : {$gt:-1, $lt:4}}
            };
}
else
{
    query = {
                "_id": { $eq: {"$oid": target} },
            };
}


var target = "";
var activeIsland = "";
var socialInfo = "";
var userResources = "";


var result = collection.update({"_id": { $eq: {"$oid": id} }}, {"$set":{"GameStatus": 210}});

var result = collection.findOne( query, {"ActiveIsland":1 , "SocialInfo" : 1, "UserResources" : 1} );
if(result)
{
    target = result["_id"].$oid;
    activeIsland = (result.ActiveIsland)? result.ActiveIsland : "";
    socialInfo = (result.SocialInfo)? result.SocialInfo : "";
    userResources = (result.UserResources)? result.UserResources : "";
    
    //update my revenge
    var query2 = 
    {
        "_id": { $eq: {"$oid": target} },
    };
    
    var result2 = collection.findOne(query2, {ActiveIsland : 1, UserResources:1})
    
    var exp = (result2.UserResources.Exp)? result2.UserResources.Exp : 0;
    var hasBuildings = false;
    
    if(result2.ActiveIsland && result2.ActiveIsland.Buildings)
        for(var n in result2.ActiveIsland.Buildings)
            if(n > -1) hasBuildings = true;
    
    UpdateExistentRevenge(target, exp, hasBuildings, id);
}

Spark.setScriptData("Id", target);
Spark.setScriptData("ActiveIsland", activeIsland);
Spark.setScriptData("SocialInfo", socialInfo);
Spark.setScriptData("UserResources", userResources);

