var id = Spark.getData().id;
var targets = Spark.getData().json.targets;

var collection = Spark.runtimeCollection("users");
var oids = [];

var update = {};
update["$set"] = {};
var spins = 0;


for(var key in targets)
{
    update.$set["Friends.".concat(targets[key]).concat(".HasEnergy")] = false;
    spins += 1;
}

update["$inc"] = {};
update.$inc["UserResources.Spins"] = spins;

var result = collection.findAndModify({"_id" : { "$eq" : {"$oid" : id}}}, {"UserResources" : 1}, {}, false, update, true, false);

Spark.setScriptData("UserResources", result.UserResources);
