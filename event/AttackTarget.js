require("MESSAGE_MODULE");
require("REVENGE_MODULE");
require("NEWS_MODULE");

var id = Spark.getData().id;
var json = Spark.getData().json;

var target = json.Target;
var islandId = json.IslandId;
var buildingId = json.BuildingId
var cash = json.Cash;
var buildingDestroyed = (json.Destroyed)? json.Destroyed : -1;

var collection = Spark.runtimeCollection("users");

//update self cash and get name / fbId

var selfUpdate = {};
selfUpdate["$set"] = {};
selfUpdate.$set = {"GameStatus" : 200};

selfUpdate["$inc"] = {};

selfUpdate.$inc["UserResources.Cash"] = cash;

selfUpdate.$inc["Stats.GoldAttacks"] = cash;
selfUpdate.$inc["Stats.TotalAttacks"] = 1;
if(buildingDestroyed == 1)
    selfUpdate.$inc["Stats.BuildingDestroyed"] = 1;
else (buildingDestroyed == 0)
    selfUpdate.$inc["Stats.BuildingsDamaged"] = 1;

var userInfo = collection.findAndModify({"_id" : { "$eq": {"$oid": id} }}, {"SocialInfo" : 1, "UserResources" : 1, "ActiveIsland" : 1}, {}, false, selfUpdate, false, false);

var socialInfo = (userInfo.SocialInfo)? userInfo.SocialInfo : {};
var exp = (userInfo.UserResources.Exp)? userInfo.UserResources.Exp : 0;

//attack other
var buildingKey = "ActiveIsland.Buildings.".concat(buildingId);

var query = {};
query["_id"] = { "$eq": {"$oid": target} };
query["ActiveIsland.Id"] = islandId;
query[buildingKey] = {"$gt":-1};

var findOne = collection.findOne(query, { ActiveIsland : 1, UserResources : 1, SocialInfo : 1 });


if(findOne)
{
    var update = {};
    
    var damaged = false;
        
    var hadShield = false;
    
    update["$inc"] = {}
    update["$inc"]["Stats.AttackReceived"] = 1;
    
    if(findOne.UserResources && findOne.UserResources.Shields && findOne.UserResources.Shields > 0)
    {
        update["$inc"]["UserResources.Shields"] = -1;
        update["$inc"]["Stats.AttackDefended"] = 1;
        
        hadShield = true;
        
        
    }
    else
    {
        if(findOne.ActiveIsland)
        {
            damaged = true;
            
            if(findOne.ActiveIsland.Damaged[buildingId] === true)
            {
                
                update["$inc"][buildingKey] = -1;
                
                if(findOne["ActiveIsland"].Buildings[buildingId] <= 0)
                {
                    damaged = false;
                    update["$inc"]["Stats.BuildingDestroyedMine"] = 1;
                }
                else
                    update["$inc"]["Stats.BuildingsDamagedMine"] = 1;
                
                
                update["$inc"]["UserResources.Exp"] = -1;

            }
            
            update["$set"] = {}
            update["$set"]["ActiveIsland.Damaged.".concat(buildingId)] = damaged;
           
        }
    }
    
    var updateSuccess = collection.findAndModify(query, {"SocialInfo" : 1}, {}, false, update, false, false);
    
    if(updateSuccess !== null)
    {
        var msgJson = {};
        
        var name = "someone"
        
        if("SocialInfo" in findOne)
            name = userInfo.SocialInfo.DisplayName;
        
        if(hadShield)
        {
            msgJson["title"] = "defended";
            msgJson["body"] =  "You defended an attack from ".concat(name); 
            msgJson["data"] = { 
                                "kind" : "defence",
                                "id" : id,
                                "name" : name
                              };
        }
        else
        {
            msgJson["title"] = "attacked";
            msgJson["body"] = name.concat("  attacked you"); 
            msgJson["data"] = { 
                                "kind" : "attack",
                                "id" : id,
                                "buildingId" : buildingId,
                                "name" : name
                              };
        }
        
        var hasBuildings = false;

        if(userInfo.ActiveIsland && userInfo.ActiveIsland.Buildings)
            for(var n in userInfo.ActiveIsland.Buildings)
                if(n > -1) hasBuildings = true;
        
        DisableRevenge(id, target);
        createAttackNews(target, id, userInfo.SocialInfo, buildingId, hadShield);
        UpdateRevenge(id, userInfo.SocialInfo, exp, hasBuildings, target, "Attacks", 1);
        
        sendMessage(target, msgJson);
    }
}