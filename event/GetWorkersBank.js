var id = Spark.getData().id;

var collection = Spark.runtimeCollection("users");

var query =  
{
        "_id": { $eq: {"$oid": id} }
};

var result = collection.findOne(query, {"WorkersBank":1, "CompletedIsland" : 1}); 

var json = {};
json["Amount"] = 0;
json["LastInteraction"] = Date.now();
json["CurrentDate"] = Date.now();

var completedIslands = {};

if(result !== null)
{
    if("WorkersBank" in result)
    {
        json["Amount"] = result["WorkersBank"].Amount;
        json["LastInteraction"] = result["WorkersBank"].LastInteraction;
    }
    
    if("CompletedIsland" in result)
        completedIslands = result.CompletedIsland;
    
}

Spark.setScriptData("WorkersBank", json);
Spark.setScriptData("CompletedIsland", completedIslands);