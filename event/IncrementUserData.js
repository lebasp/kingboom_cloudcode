var id = Spark.getData().id;
var key = Spark.getData().key;
var value = Spark.getData().value;

var collection = Spark.runtimeCollection("users");
var query =  {"_id": { $eq: {"$oid": id} }};

var json = {};
json[key] = value;

if(key === "UserResources.Spins")
{
    if(value > 0)
        json["Stats.TotalSpins"] = value;
}


var update = {"$inc":json};

var result = collection.findAndModify(query, {"UserResources" : 1}, {}, false, update, true, false);

Spark.setScriptData("result", result.UserResources);