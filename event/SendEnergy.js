var id = Spark.getData().id;
var targets = Spark.getData().json.targets;

var collection = Spark.runtimeCollection("users");
var oids = [];

var update = {};
update["$set"] = {};
for(var key in targets)
{
    update.$set["Friends.".concat(targets[key]).concat(".LastTimeSent")] = Date.now();
    oids.push({"$oid":targets[key]});
}

collection.update({"_id" : { "$eq" : {"$oid" : id}}}, update)

var updateOther = {};
updateOther["$set"] = {};
updateOther.$set["Friends.".concat(id).concat(".HasEnergy")] = true;


collection.update({ "_id": { $in: oids }}, updateOther);
