var id = Spark.getData().id;
var json = Spark.getData().json;

var collection = Spark.runtimeCollection("users");

var query =  {"_id": { $eq: {"$oid": id} }};


var result = collection.findOne(query, { "Treasures" : 1 });
var chests = json.Chests;

var indexProgress = 0;
if("IndexProgress" in json)
    indexProgress = json.IndexProgress;

if(result.Treasures)
{
    if(result.Treasures.Chests)
    {    
        var savedChests = result.Treasures.Chests;
        
        var lenght = (savedChests.length > chests.length)? savedChests.length : chests.length;
        
        if(savedChests)
        {
            for (var i=0; i < lenght; i++) 
            {
                if(i < chests.length && i < savedChests.length)
                {
                    chests[i] += savedChests[i];
                }
                else
                {
                    if(savedChests[i])
                        chests.push(savedChests[i]);
                }
            }
        }
    }
}

Spark.setScriptData("result", chests);

for (var i=0; i < chests.length; i++) 
    if(chests[i] > 10) chests[i] = 10;

var update ={
                "Treasures.MoneySpent" : json.MoneySpent,
                "Treasures.Chests" : chests,
                "Treasures.IndexProgress" : indexProgress
            };

collection.update(query, {"$set" : update });
