var id = Spark.getData().id;
var key = Spark.getData().key;
var value = Spark.getData().value;
var force = Spark.getData().force;

var collection = Spark.runtimeCollection("users");

var query = {};
query["_id"] = { "$eq": {"$oid": id} };
if(!force)
    query[key] = { "$exists" : false };

var json = {};
json[key] = value;

var update = {"$set":json};

collection.update(query, update);