var id = Spark.getData().id;
var keys = Spark.getData().json.keys;

var collection = Spark.runtimeCollection("users");

var json = {}
for(var key in keys)
    json[ keys[key] ] = 1;

var query =  {"_id": { $eq: {"$oid": id} }};

var result = collection.findOne(query, json);

for(var key in keys)
    Spark.setScriptData(keys[key], result[ keys[key] ] );