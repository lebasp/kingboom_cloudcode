var id = Spark.getData().id;
var json = Spark.getData().json;

var collection = Spark.runtimeCollection("users");
var spins = json["Spins"];


var query = {"_id" : { "$eq": {"$oid": id} }};
var update = { "$inc" : { "UserResources.Spins" : spins, "Stats.TotalSpins" : spins }, "$set" : {"NextSpinEarned" : json["NextSpinEarned"]}};

var result = collection.findAndModify(query, {"UserResources" : 1}, {}, false, update, true, false);

Spark.setScriptData("UserResources", result.UserResources);

