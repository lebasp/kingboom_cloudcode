var id = Spark.getData().id;
var keys = Spark.getData().intValue1;
var keySeconds = Spark.getData().intValue2;

var collection = Spark.runtimeCollection("users");

collection.update({"_id" : { "$eq": {"$oid": id} }}, {"$set"  : { "Treasures.Keys" : keys, "Treasures.KeySeconds" : keySeconds, "Treasures.LastInteraction" : Date.now()  }});