var id = Spark.getData().id;
var json = Spark.getData().json;

var collection = Spark.runtimeCollection("users");

var query =  {"_id": { $eq: {"$oid": id} }};

var update = {};
update.$inc = {};

for(var resource in json.UserResources)
{
    update.$inc["UserResources.".concat(resource)] = json.UserResources[resource];
}

for(var i=0; i < json.Chests.length ; i++)
    update.$inc["Treasures.Chests.".concat(i)] = json.Chests[i];
update.$inc["Treasures.Keys"] = json.Keys;

var result = collection.findAndModify(query, { "UserResources" : 1 }, {}, false, update, true, false);

Spark.setScriptData("result", result.UserResources);