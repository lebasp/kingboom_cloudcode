var fbids = Spark.getData().strList;

var collection = Spark.runtimeCollection("users");

var result = collection.find( { "SocialInfo.FacebookId": { $in: fbids } }, {"SocialInfo" : 1, "UserResources" : 1, "ActiveIsland" : 1});

Spark.setScriptData("result", result);