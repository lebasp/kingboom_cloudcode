require("MESSAGE_MODULE"); 
require("REVENGE_MODULE"); 
require("NEWS_MODULE");

var id = Spark.getData().id;
//old version
var target = Spark.getData().target;
var cash = Spark.getData().intValue;
var foundCashKing = false;

//new version
var json = Spark.getData().json;
if(target === "null")
{
    target = json.Target;
    cash = json.Cash;
    foundCashKing = json.FoundCashKing;
}



var collection = Spark.runtimeCollection("users");

//update self cash
var userInfo = collection.findAndModify({"_id" : { "$eq": {"$oid": id} }}, {"UserResources" : 1, "SocialInfo" : 1, "ActiveIsland" : 1}, {}, false, {"$set":{"GameStatus" : 200},"$inc"  : {"Stats.GoldSteal" : cash , "Stats.TotalSteal": 1, "UserResources.Cash" : cash }}, true, false);
var userSocialInfo = (userInfo.SocialInfo)? userInfo.SocialInfo : {}

var missed = false;
//remove cash king
if(id !== "dummy")
{
    var updated = collection.findAndModify({
                                        "_id" : { "$eq": {"$oid": id} },
                                        "CashKing.Id": target
                                    }, {"$unset"  : { "CashKing" : {} }});
    
}

if(foundCashKing)
    collection.update({ "_id" : { "$eq": {"$oid": id} }}, {"$inc" : {"Stats.CaughtCashking" : 1}});
else
    collection.update({"_id" : { "$eq": {"$oid": id} }}, {"$inc" : {"Stats.MissedCashking" : 1}});

//update 
if(target && target !== "dummy")
{
    var otherQuery = {
                        "_id" : { $eq: {"$oid": target} },
                        "UserResources.Cash" : {$gt : 100000},
                        $or:[ {"Immunity" : {$exists:false}} , {"Immunity" : false} ]
                     };
    
    var otherFields = {};
    //otherFields [ "Revenges.".concat(id) ] = 1; <== no idea what was going on here
    otherFields["UserResources.Cash"] = 1;
    otherFields["SocialInfo"] = 1;
    
    var otherResult = collection.findAndModify( otherQuery, otherFields, {},false, {"$mul" : { "UserResources.Cash" : 0.15 }, "$inc" : {"Stats.TimesStolen" : 1}}, false, false);
    
    if(otherResult !== null)
    {
        var cashStolen = Math.floor( otherResult["UserResources"].Cash * 0.85 );
        
        var hasBuildings = false;
        
        if(userInfo.ActiveIsland && userInfo.ActiveIsland.Buildings)
            for(var n in userInfo.ActiveIsland.Buildings)
                if(n > -1) hasBuildings = true;
        
        createStealNews(target, id, userSocialInfo, cashStolen);
        UpdateRevenge(id, userSocialInfo, userInfo.UserResources.Exp, hasBuildings, target, "CashStolen", cashStolen );
        
        var name = "someone"
        
        if("SocialInfo" in userInfo)
            name = userInfo.SocialInfo.DisplayName;
        
        var msgJson = [];
        msgJson["title"] = "attacked";
        msgJson["body"] = name.concat("  stolen ".concat(cashStolen).concat(" from you")); 
        msgJson["data"] = {
                            "kind" : "steal",
                            "id" : id,
                            "amount" : cashStolen,
                            "name" : name
                          };
        
        sendMessage(target, msgJson);
    }
}

Spark.setScriptData("result", userInfo.UserResources.Cash);