// ====================================================================================================
//
// Cloud Code for AuthenticationResponse, write your code here to customize the GameSparks platform.
//
// For details of the GameSparks Cloud Code API see https://docs.gamesparks.com/
//
// ====================================================================================================

var player = Spark.getPlayer();
var id = player.getPlayerId();


var collection = Spark.runtimeCollection("users");
var result = collection.findOne({"_id":{"$oid": id}, "SocialInfo.FacebookId":{"$exists" : true}}, {"SocialInfo" : 1})

if(result)
{
    Spark.setScriptData("FacebookId", result.SocialInfo.FacebookId);
}

